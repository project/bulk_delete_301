CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------
  
 * The Bulk delete 301 provide the functionality of deleting 301 redirects 
   by csv upload  
    - Option for preventing url from 404.
   
 For a full description of the module, visit the project page:
  - https://www.drupal.org/sandbox/alokvermaei/2330429

REQUIREMENTS
------------
This module requires the following modules:
 * Redirect (Core module)

INSTALLATION
------------
 * Install as you would normally install a contributed drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------
 * Configure the modules settings in Administration >> Configuration >>
   URL redirects >> Bulk Delete 301
    
MAINTAINER
-----------
Current maintainers:
 * Alok Kumar Verma (alokvermaei) - https://www.drupal.org/user/1573680
